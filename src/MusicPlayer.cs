using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*** Music Player ***
 * Requires: AudioSource
 * Function: play audio if preferences have audio ON
 * 
 * Notes: Including this class in a scene allows music playback
 * 
 */

public class MusicPlayer : MonoBehaviour {

    //public MusicSettings musicSettings;
    public AudioSource[] myBattleAudio;
    public AudioSource finalBattleAudio;
    public AudioSource menuAudio;

    private static MusicPlayer thisMusicPlayer = null;
    private float originalVolume;

    private int index;

	// Use this for initialization
	void Start ()
    {
        if (thisMusicPlayer == null)
        {
            thisMusicPlayer = this;
            DontDestroyOnLoad(thisMusicPlayer);

            index = Random.Range(0, myBattleAudio.Length);
            originalVolume = myBattleAudio[index].volume;
            CheckTogglePlayer();
        }
        else
            Destroy(this.gameObject);

        if (!PlayerPrefs.HasKey("music"))
        {
            PlayerPrefs.SetInt("music", 1);
            PlayerPrefs.Save();
        }

    }

    public void StopMusicPlayer()
    {
        StartCoroutine(FadeOut(myBattleAudio[index]));
    }

    public void StartMusicPlayer()
    {
        StartCoroutine(FadeIn(myBattleAudio[index]));
    }

    public void PlayFinalBattleMusic()
    {
        if (PlayerPrefs.GetInt("music") == 1)
        {
            StartCoroutine(FadeOut(myBattleAudio[index]));
            StartCoroutine(FadeIn(finalBattleAudio));
        }
    }

    public void PlayMenuMusic()
    {
        finalBattleAudio.Stop();

        if (PlayerPrefs.GetInt("music") == 1)
        {
            StartCoroutine(FadeOut(myBattleAudio[index]));

            menuAudio.volume = 1;
            menuAudio.Play();
        }
    }

    public void PlayBattleMusic()
    {
        finalBattleAudio.Stop();
        index = Random.Range(0, myBattleAudio.Length);

        if (PlayerPrefs.GetInt("music") == 1)
        {
            StartCoroutine(FadeOut(menuAudio));
            StartCoroutine(FadeIn(myBattleAudio[index]));
        }
    }

    IEnumerator FadeOut(AudioSource fadeOutMusic)
    {
        if (fadeOutMusic == menuAudio)
        {
            while (fadeOutMusic.volume > 0)
            {
                fadeOutMusic.volume -= Time.deltaTime;
                yield return null;
            }
        }
        else
        {
            while (fadeOutMusic.volume > 0)
            {
                fadeOutMusic.volume -= Time.deltaTime * originalVolume;
                yield return null;
            }
        }
        fadeOutMusic.Stop();
    }

    IEnumerator FadeIn(AudioSource fadeInMusic)
    {
        fadeInMusic.volume = 0;
        fadeInMusic.Play();

        while (fadeInMusic.volume < originalVolume)
        {
            fadeInMusic.volume += Time.deltaTime * originalVolume;
            yield return null;
        }
    }


    public void CheckTogglePlayer()
    {
        if (PlayerPrefs.GetInt("music") == 1)
        {
            PlayMenuMusic();
        }
        else
        {
            menuAudio.Stop();
        }
    }
	
}
