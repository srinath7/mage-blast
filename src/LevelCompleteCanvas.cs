using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelCompleteCanvas : MonoBehaviour
{
    public GameStats gameStats;
    public Canvas completeCanvas;
    public Text[] playerName;
    public Text[] kills;

    private float startingPosition;
    private float lerpToPosition;

    // Use this for initialization
    void Start()
    {
        startingPosition = Screen.height * 2;

        lerpToPosition = startingPosition;
    }

    private void Update()
    {
        LerpCanvas(lerpToPosition);
    }

    public void ActivateCompletionCanvas()
    {
        int i = 0;

        while (i < gameStats.GetTotalRanks())
        {
            playerName[i].text = gameStats.GetNameByRank(i + 1);
            kills[i].text = gameStats.GetKillsByRank(i + 1).ToString();

            i++;
        }

        lerpToPosition = 0;
    }

    public void ExitCompletionCanvas()
    {
        lerpToPosition = startingPosition;
    }

    private void LerpCanvas(float moveToY)
    {
        float newY = Mathf.Lerp(completeCanvas.GetComponent<RectTransform>().anchoredPosition.y,
            moveToY, Time.deltaTime * 12f);
        Vector2 newPosition = new Vector2(completeCanvas.GetComponent<RectTransform>().anchoredPosition.x, newY);

        completeCanvas.GetComponent<RectTransform>().anchoredPosition = newPosition;
    }
}

