using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public struct MageData
{
    public int id;
    public float x;
    public float y;
    public int hp;
    public int mana;
    public bool isShieldOn;
};
