using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TitleColliderFX : MonoBehaviour
{
    public AudioSoundFX audioSoundFX;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    void OnTriggerEnter2D(Collider2D trigger)
    {
        audioSoundFX.PlayOnHitFX();
    }
}
