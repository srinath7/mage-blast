using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/*** Music Settings ***
 * Requires: Button with changeable image
 * Function: Creates a preference "music" if one does not exist, and initializes ON
 * Else defaults to current player preference.  Loads on/off sprite to button image.
 * 
 * 
 * Notes: Each button click should invoke the toggle function, which will toggle button image
 * and change the saved preference
 * 
 */

public class MusicSettings : MonoBehaviour {

    public Text musicButtonText;
    private Color originalTextColor;
    private bool musicOn;

    public void Awake()
    {
        originalTextColor = musicButtonText.color;

        // If the music value has never been saved, then create it.  Default to true.  Turn toggle on

        if (!PlayerPrefs.HasKey("music"))
        {
            PlayerPrefs.SetInt("music", 1);
            PlayerPrefs.Save();
            musicOn = true;
            musicButtonText.color = originalTextColor;
        }

        if (PlayerPrefs.GetInt("music") == 0)
        {
            musicOn = false;
            musicButtonText.color = originalTextColor / 2;
        }
        else
        {
            musicOn = true;
            musicButtonText.color = originalTextColor;
        }

    }

    public void ToggleMusic()
    {
        if (musicOn)
        {
            TurnOffMusic();
        }
        else
        {
            TurnOnMusic();
        }

        GameObject.FindObjectOfType<MusicPlayer>().CheckTogglePlayer();

    }

    void TurnOnMusic ()
    {
        musicButtonText.color = originalTextColor;
        musicOn = true;
        PlayerPrefs.SetInt("music", 1);
        PlayerPrefs.Save();
    }

    void TurnOffMusic()
    {
        musicButtonText.color = originalTextColor/2;
        musicOn = false;
        PlayerPrefs.SetInt("music", 0);
        PlayerPrefs.Save();
    }
}
