using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CanvasSlider : MonoBehaviour
{
    public Canvas thisCanvas;

    public bool slideFromTop;
    public bool startFromRightHere;

    protected float startingPosition;

    protected float lerpToPosition;
    private bool isVisible;

    // Use this for initialization
    void Start()
    {
        thisCanvas = this.GetComponent<Canvas>();

        if (slideFromTop)
        {
            if (startFromRightHere)
            {
                startingPosition = thisCanvas.GetComponent<RectTransform>().anchoredPosition.y;
            }
            else
            {
                startingPosition = Screen.height * 1.5f;
            }
        }
        else
        {
            if (startFromRightHere)
            {
                startingPosition = thisCanvas.GetComponent<RectTransform>().anchoredPosition.x;
            }
            else
            {
                startingPosition = Screen.width * 1.5f;
            }
        }

        isVisible = false;
        lerpToPosition = startingPosition;
    }

    private void Update()
    {
        LerpCanvas();
    }

    public void ActivateCanvas()
    {
        lerpToPosition = 0;
        isVisible = true;
    }

    public void ExitCanvas()
    {
        lerpToPosition = startingPosition;
        StartCoroutine(RemoveAfter1());
    }

    IEnumerator RemoveAfter1()
    {   // this prevents the click on the button from allowing the tongue to shoot.  without a delay the click to hit teh button also triggers the mousebutton released logic (throw tongue)
        yield return new WaitForSeconds(0.1f);

        isVisible = false;
    }

    private void LerpCanvas()
    {
        float newY;
        float newX;

        if (slideFromTop)
        {
            newX = 0;
            newY = Mathf.Lerp(thisCanvas.GetComponent<RectTransform>().anchoredPosition.y, lerpToPosition, Time.deltaTime * 10f);
        }
        else
        {
            newX = Mathf.Lerp(thisCanvas.GetComponent<RectTransform>().anchoredPosition.x, lerpToPosition, Time.deltaTime * 10f);
            newY = 0;
        }

        thisCanvas.GetComponent<RectTransform>().anchoredPosition = new Vector2(newX, newY);
    }

    public bool IsVisible()
    {
        return isVisible;
    }
}

