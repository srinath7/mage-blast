using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharM3 : MonoBehaviour
{
    protected Animator animator;
    protected Rigidbody2D rB2D;
    protected Camera cam;
    protected AudioSoundFX audioSoundFX;
    protected GameStats gameStats;

    public EAction eAction; 

    protected float time;
    protected string[] animations;

    protected int index;

    protected float startingDrag;

    protected int hp;
    protected int mana;
    protected int ac;

    protected int maxHp;
    protected int maxMana;

    protected string looking;
    protected int id;

    protected bool dead;

    // Use this for initialization
    protected void Inits()
    {
        animator = GetComponent<Animator>();
        cam = FindObjectOfType<Camera>();
        audioSoundFX = FindObjectOfType<AudioSoundFX>();
        gameStats = FindObjectOfType<GameStats>();

        index = 0;
        rB2D = GetComponent<Rigidbody2D>();
        startingDrag = rB2D.drag;
        looking = "right";
        dead = false;

        id = this.GetInstanceID();
    }

    protected void Look(Vector2 direction)
    {
        if (direction.x == 1) // To the right
        {
            transform.localScale = new Vector3(1, transform.localScale.y, transform.localScale.z);
            looking = "right";
        }
        else if (direction.x == -1) // To the left
        {
            transform.localScale = new Vector3(-1, transform.localScale.y, transform.localScale.z);
            looking = "left";
        }
    }

    protected void LookAtOpponent(Vector2 direction)
    {
        if (direction.x > transform.position.x) // To the right
        {
            transform.localScale = new Vector3(1, transform.localScale.y, transform.localScale.z);
            looking = "right";
        }
        else if (direction.x < transform.position.x) // To the left
        {
            transform.localScale = new Vector3(-1, transform.localScale.y, transform.localScale.z);
            looking = "left";
        }
    }

    protected void Look(int angle)
    {
        if (angle < 180 && angle > 0) // To the right
        {
            transform.localScale = new Vector3(1, transform.localScale.y, transform.localScale.z);
            looking = "right";
        }
        else if (angle > 180 && angle < 360) // To the left
        {
            transform.localScale = new Vector3(-1, transform.localScale.y, transform.localScale.z);
            looking = "left";
        }
    }

    void MoveInThisDirection(Vector2 thisDirection)
    {
        Look(thisDirection);
        rB2D.velocity = thisDirection;
    }

    public void TakeSpellAction(EAction action, Vector2 location)
    {
        if (!dead)
        {
            switch (action)
            {
                default:
                    TakeClassAction(action, location);
                    break;
            };
        }
    }

    public void TakeMoveAction(int details)
    {
        if (!dead)
        {
            animator.Play("run");
            rB2D.drag = startingDrag;
            MoveInThisDirection(Direction(details));
        }
    }

    protected virtual void TakeClassAction(EAction action, Vector2 location)
    {
        Debug.Log("TakeClassAction not overridden - check child class");
        animator.Play("idle");
        rB2D.drag = startingDrag * 100;
    }

    void OnTriggerEnter2D(Collider2D trigger)
    {
        if (id != trigger.GetComponent<Spell>().GetCaster())
        {
            CalculateDamage(trigger);

            if (hp < 1)
            {
                gameStats.AddKillCount(trigger.GetComponent<Spell>().GetCaster());
                gameStats.SetRank(id);
                audioSoundFX.PlayOnDeathFX();
                dead = true;
                rB2D.drag = startingDrag * 100;
                animator.Play("dying");
                Destroy(GetComponent<BoxCollider2D>());
            }
            else
            {
                audioSoundFX.PlayOnHitFX();
            }
        }
    }

    protected virtual void CalculateDamage(Collider2D trigger)
    {
        hp -= trigger.GetComponent<Spell>().damage;
        mana -= trigger.GetComponent<Spell>().manaBurn;

        if (hp < 0)
            hp = 0;
        if (mana < 0)
            mana = 0;
    }

    public int GetId()
    {
        id = this.GetInstanceID();
        return id;
    }

    public int GetHP()
    {
        return hp;
    }

    public int GetMana()
    {
        return mana;
    }

    public int GetMaxHp()
    {
        return maxHp;
    }

    public int GetMaxMana()
    {
        return maxMana;
    }

    public Vector3 GetLocation()
    {
        return transform.position;
    }

    protected Vector3 Direction(int angle)
    {
        while (angle > 360)
        {
            angle = angle - 360;
        }

        if (angle < 23)
            return new Vector3(0, 1);
        else if (angle < 68)
            return new Vector3(1, 1);
        else if (angle < 113)
            return new Vector3(1, 0);
        else if (angle < 158)
            return new Vector3(1, -1);
        else if (angle < 203)
            return new Vector3(0, -1);
        else if (angle < 248)
            return new Vector3(-1, -1);
        else if (angle < 293)
            return new Vector3(-1, 0);
        else if (angle < 338)
            return new Vector3(-1, 1);
        else
            return new Vector3(0, 1);

    }
}
