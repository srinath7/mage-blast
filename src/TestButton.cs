using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TestButton : MonoBehaviour
{
    public InputField actionText;
    public CharM3_Mage m3Mage;

    public bool move;
    public bool frostbolt;
    public bool snowball;
    public bool drainmana;
    public bool iceshield;
    public bool blink;
    public bool nothing;

    public void OnClick()
    {
        if (move)
            m3Mage.TakeMoveAction(System.Convert.ToInt32(actionText.text.ToString()));
        else if (blink)
            m3Mage.CastBlinkAction(System.Convert.ToInt32(actionText.text.ToString()));
        else if (frostbolt)
            m3Mage.TakeSpellAction(EAction.frostbolt, new Vector2(5,5));
        else if (snowball)
            m3Mage.TakeSpellAction(EAction.snowball, new Vector2(5, 5));
        else if (drainmana)
            m3Mage.TakeSpellAction(EAction.drainmana, new Vector2(5, 5));
        else if (iceshield)
            m3Mage.TakeSpellAction(EAction.iceshield, new Vector2(5, 5));
        else if (nothing)
            m3Mage.TakeSpellAction(EAction.nothing, new Vector2(5, 5));
    }
}
