using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

/*** Save Data ***
 * Requires: Save Manager to control creating, reading, writing, updating save data
 * Function: data class/copybook/structure use to store all applicable data elements used in a saved game 
 * 
 * Notes: 
 */

[System.Serializable]
public class SaveData
{
    public string mageName;
    public string customJS;

    /*
    public float highScore = 0;
    public int highestHangryWave = 0;

    public bool clickMethod = false;

    public bool tutorialComplete_Campaign = false;
    public bool tutorialComplete_Timer_Tap = false;
    public bool tutorialComplete_Timer_Throw = false;
    public bool[] showBiomeTransition = new bool[10];

    public int life_count_regular = 3;
    public DateTime lifeResetTimeStamp;

    public int last_page = 1;
    public int current_level = 1;
    public int[] stars_per_level = new int[1000];
    public int[] points_per_level = new int[1000];

    public bool[] trophyWon = new bool[50];

    public int starBank = 0;
    public int powerUp_Hangry = 0;
    public int powerUp_Freeze = 0;
    public int powerUp_Swollen = 0;

    public int stars_hangry_lifetime = 0;

    public int bugKills;

    public bool[] bug_Tutorial = new bool[100];

    public int biome = 1;

    public bool[] achievementWon = new bool[100];
    */
}
