﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayControllerJS : MonoBehaviour
{
    public AccessSaveData accessSaveData;
    public LevelCompleteCanvas levelCompleteCanvas;
    public MageEntrySetup mageEntrySetup;
    public MusicPlayer musicPlayer;
    public DebugLog debugLog;

    public CharM3_Mage charM3_Mage;
    public MageControllerJS prefabMageController;

    private int mageArrayCount;

    private float turnDuration = 2;
    private float turnTimer;
    private bool movedThisTurnAlready;
    private bool playingRegMusic;
    private bool gameOver;

    private CharM3_Mage[] physicalMage;
    private MageControllerJS[] mageControllerJS;
    private int[] id;
    private GameObject[] nameTag;

    public GameStats gameStats;
    public GameObject nameLabelPrefab;

    // Start is called before the first frame update
    void Start()
    {
        // this needs to coordinate with CountDownTimer class
        turnTimer = -6;
        mageArrayCount = 11;

        id = new int[mageArrayCount];
        nameTag = new GameObject[mageArrayCount];
        physicalMage = new CharM3_Mage[mageArrayCount];
        mageControllerJS = new MageControllerJS[mageArrayCount];
        movedThisTurnAlready = false;
        gameOver = false;

        musicPlayer = FindObjectOfType<MusicPlayer>();
        playingRegMusic = true;

        for (int i = 1; i < mageArrayCount; i++)
        {
            if (mageEntrySetup.BucketActive(i))
            {
                physicalMage[i] = Instantiate(charM3_Mage, new Vector3(5, 5, 1), transform.rotation);
                mageControllerJS[i] = Instantiate(prefabMageController, new Vector3(5, 5, 1), transform.rotation);

                if (mageEntrySetup.BucketUseBot(i))
                {   // bucket [0] is the default dummy JS.  Access [0] if the user wants a BOT and not a custom mage script
                    mageControllerJS[i].SetScripts(accessSaveData.GetJS(0));
                }
                else
                {
                    mageControllerJS[i].SetScripts(accessSaveData.GetJS(i));
                }

                mageControllerJS[i].Start();
                id[i] = physicalMage[i].GetId();
                SetNameTag(i);
                gameStats.InitMage(id[i], accessSaveData.GetMageName(i));
            }
        }

        RandomizeMageLocation();
    }

    private void RandomizeMageLocation()
    {
        for (int i = 0; i < mageArrayCount; i++)
        {
            if (mageEntrySetup.BucketActive(i))
            {
                physicalMage[i].transform.position = new Vector2(
                UnityEngine.Random.Range(2, 13),
                UnityEngine.Random.Range(1.6f, 7.0f));
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        MoveNameTags();
        turnTimer += Time.deltaTime;

        if (turnTimer > turnDuration)
        {
            if (IsGameOver())
            {
                levelCompleteCanvas.ActivateCompletionCanvas();
            }
            else
            {
                Mages_Reset();
                Mages_LoadData();
                Mages_CalcActions();
                Mages_SpellActions();
                debugLog.PrintLog();

                turnTimer = 0;
                movedThisTurnAlready = false;
            }
        }
        else if (turnTimer > turnDuration / 2 && !movedThisTurnAlready)
        {
            Mages_MoveActions();

            movedThisTurnAlready = true;
        }
        else
        {
            // do nothing
        }
    }

    private bool IsGameOver()
    {
        if (gameOver)
            return true;
        else
        {
            int playersAlive = 0;

            for (int i = 0; i < mageArrayCount; i++)
            {
                if (physicalMage[i] != null)
                    if (physicalMage[i].GetHP() > 0)
                        playersAlive++;
            }

            if (playersAlive < 3 && playingRegMusic)
            {
                musicPlayer.PlayFinalBattleMusic();
                playingRegMusic = false;
            }

            if (playersAlive > 1)
                return false;
            else
            {
                gameOver = true;
                musicPlayer.PlayBattleMusic();
                return true;
            }
        }
    }

    private void Mages_MoveActions()
    {
        for (int i = 0; i < mageArrayCount; i++)
        {
            if (physicalMage[i] != null)
            {
                if (mageControllerJS[i].GetMoveThisTurn())
                {
                    physicalMage[i].TakeMoveAction(mageControllerJS[i].GetMovementAngle());
                }
                else
                {
                    if (mageControllerJS[i].GetTurnAction() == EAction.nothing)
                        physicalMage[i].AddMana(3);
                }
            }
        }
    }

    private void Mages_SpellActions()
    {
        // TODO randomize action order
        // do action
        for (int i = 0; i < mageArrayCount; i++)
        {
            if (physicalMage[i] != null)
            {
                if (mageControllerJS[i].GetTurnAction() == EAction.blink)
                {
                    // blink spell uses movementAngle instead of attackID as input parameter
                    physicalMage[i].CastBlinkAction(mageControllerJS[i].GetMovementAngle());
                }
                else if (mageControllerJS[i].GetTurnAction() == EAction.iceshield)
                {
                    // iceshield spell uses movementAngle instead of attackID as input parameter
                    physicalMage[i].TakeSpellAction(EAction.iceshield, new Vector2(0,0));
                }
                else if (mageControllerJS[i].GetTurnAction() == EAction.nothing)
                {
                    // iceshield spell uses movementAngle instead of attackID as input parameter
                    physicalMage[i].TakeSpellAction(EAction.nothing, new Vector2(0, 0));
                }
                else
                {
                    // NON-blink spells use attackID
                    physicalMage[i].TakeSpellAction(mageControllerJS[i].GetTurnAction(), 
                        ConvertIDtoLocation(mageControllerJS[i].GetAttackID()));


                    if (mageControllerJS[i].GetAttackID() == 0)
                    {
                        Debug.Log("Mage id: " + id[i]);
                        Debug.Log("mageControllerJS[i].GetTurnAction(): " + mageControllerJS[i].GetTurnAction());
                    }
                }
            }
        }
    }

    private Vector2 ConvertIDtoLocation(int idIn)
    {
        int i = 0;
        bool keepLooping = true;

        while (keepLooping)
        {
            if (idIn == id[i])
                if (physicalMage[i] != null)
                {
                    return new Vector2(physicalMage[i].GetLocation().x, physicalMage[i].GetLocation().y);
                }
                else
                {
                    Debug.Log("Attack ID has already been killed! - ID: " + idIn);
                    return new Vector2(6.7f, 5);
                }
            else
            {
                i++;
                if (i >= id.Length)
                    keepLooping = false;
            }
        }

        return new Vector2(6.7f, 5);
    }

    private void Mages_CalcActions()
    {
        for (int i = 0; i < mageArrayCount; i++)
        {
            if (mageControllerJS[i] != null)
                mageControllerJS[i].ExecuteScripts();
        }
    }

    private void Mages_LoadData()
    {
        // currentMage is used to pull data from mageArray, then loaded back into main array
        MageData currentMage = new MageData();

        for (int i = 0; i < mageArrayCount; i++)
        {
            if (physicalMage[i] != null)
            {
                currentMage.id = physicalMage[i].GetId();
                currentMage.hp = physicalMage[i].GetHP();
                currentMage.mana = physicalMage[i].GetMana();
                currentMage.x = physicalMage[i].GetLocation().x;
                currentMage.y = physicalMage[i].GetLocation().y;
                currentMage.isShieldOn = physicalMage[i].IsShieldOn();

                mageControllerJS[i].LoadMyMageData(currentMage);
            }
        }

        for (int i = 0; i < mageArrayCount; i++)
        {
            if (physicalMage[i] != null)
            {
                currentMage.id = physicalMage[i].GetId();
                currentMage.hp = physicalMage[i].GetHP();
                currentMage.mana = physicalMage[i].GetMana();
                currentMage.x = physicalMage[i].GetLocation().x;
                currentMage.y = physicalMage[i].GetLocation().y;
                currentMage.isShieldOn = physicalMage[i].IsShieldOn();

                for (int j = 0; j < mageArrayCount; j++)
                {
                    if (mageControllerJS[j] != null)
                    {
                        if (currentMage.id != id[j] && currentMage.hp > 0)
//                        if (currentMage.id != id[j])
                        {
                            //Debug.Log("MagesLoadData: " + currentMage.id + " " + id[j]);
                            mageControllerJS[j].LoadListData(currentMage);
                        }
                    }
                }
            }
        }
    }

    private void MoveNameTags()
    {
        for (int i = 0; i < mageArrayCount; i++)
            if (physicalMage[i] != null)
                nameTag[i].transform.position = physicalMage[i].GetLocation() + new Vector3(0.1f, 0.5f, -3);
    }

    private void Mages_Reset()
    {
        // let each algorithm know a new turn has started
        // call PLUGIN to getAction
        for (int i = 0; i < mageArrayCount; i++)
        {
            if (mageControllerJS[i] != null)
               mageControllerJS[i].NewTurn();
        }
    }

    // *******************************************************************************************************
    //
    // Access methods - These will need updated if more than 10 mages are needed
    //
    //********************************************************************************************************

    private void SetNameTag(int index)
    {
        nameTag[index] = Instantiate(nameLabelPrefab, new Vector3(0,0,-3), transform.rotation);
        nameTag[index].GetComponent<TextMesh>().text = accessSaveData.GetMageName(index);
    }
}
